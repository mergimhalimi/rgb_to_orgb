/****************************************************************
   FILE:      color_adjustment.cpp
   AUTHOR:    Mergim Halimi
   DUE DATE:

   PURPOSE:   The implementations of the functions in class
              ColorAdjustment
****************************************************************/

//Class
#include "color_adjustment.h"

ColorAdjustment::ColorAdjustment()
{
  /*Empty constructor.*/
}

// oRGB image expressed as double values
// L   [ 0, 1]
// Crg [-1, 1]
// Cyb [-1, 1]
ColorAdjustment::ColorAdjustment(cv::Mat image_)
{
  this->image_ = image_;
}

cv::Mat ColorAdjustment::adjustColor(Set deviate_, double value_)
{
  cv::Mat deviated_image_ = this->image_.clone();
  
  cv::MatIterator_<cv::Vec3d> it, end;

  for (it = deviated_image_.begin<cv::Vec3d>(), end = deviated_image_.end<cv::Vec3d>(); it != end; ++it) 
  {
    double &L   = (*it)[0];
    double &Crg = (*it)[2];
    double &Cyb = (*it)[1];

    // std::cout << "Readen pixel : [" << L << ", " 
    //                        << Crg << ", " 
    //                        << Cyb << "]" 
    //                        << std::endl;

    //Main idea is to change the value of Crg or Cyb in order to cool or warm image
    //For example if we have an image with this pixel oRG=[0.5, 0.4, 0.3] and we want to
    //warm it in red green axis, we subtract value from Crg channel. 0.2 warming in Crg
    //should give a result of [0.5, 0.2, 0.3]. If values exceed limits, than we truncate it.
    switch(deviate_)
    {
      case Set::L :
      {
        // To warm or cool image, we only use Crg or Cyb channels
        // r = cv::saturate_cast<uchar>((alpha_ * calculated_alpha_.second(2) * r) + beta_);
        //std::cout << "R\n";
        break;
      }
      case Set::Crg :
      {
        //std::cout << "Value before trans: " << Cyb << std::endl;
        Crg -= value_;
        //std::cout << "Value for transform in Cyb: " << value_ << std::endl;
        break;
      }
      case Set::Cyb :
      {
        Cyb -= value_; 
        //std::cout << "Value for transform in Crg: " << value_ << std::endl;
        break;
      }
      default :
        break;
    }

    // std::cout << "Transformed pixel : [" << L << ", " 
    //                         << Crg << ", " 
    //                         << Cyb << "]" 
    //                         << std::endl;
    // std::cin.get();

  }
  return deviated_image_;
}

cv::Mat ColorAdjustment::adjustColor(double L, double Crg, double Cyb, Set exclude_)
{
  cv::Mat deviated_image_ = this->image_.clone();
  
  cv::MatIterator_<cv::Vec3d> it, end;
  
  for (it = deviated_image_.begin<cv::Vec3d>(), end = deviated_image_.end<cv::Vec3d>(); it != end; ++it) 
  {
    double &L_   = (*it)[0];
    double &Crg_ = (*it)[2];
    double &Cyb_ = (*it)[1];

    // std::cout << "Readen pixel : [" << L_ << ", " 
    //                        << Crg_ << ", " 
    //                        << Cyb_ << "]" 
    //                        << std::endl;

    //Main idea is to change the value of Crg or Cyb in order to cool or warm image
    //For example if we have an image with this pixel oRG=[0.5, 0.4, 0.3] and we want to
    //warm it in red green axis, we subtract value from Crg channel. 0.2 warming in Crg
    //should give a result of [0.5, 0.2, 0.3]. If values exceed limits, than we truncate it.
    switch(exclude_)
    {
      case Set::L :
      {
        //std::cout << "L Excluded" << std::endl;
        Crg_ = Crg;
        Cyb_ = Cyb;
        break;
      }
      case Set::Crg :
      {
        //std::cout << "Crg Excluded" << std::endl;
        L_ = L;
        Cyb_ = Cyb;
        break;
      }
      case Set::Cyb :
      {
        //std::cout << "Cyb Excluded" << std::endl;
        L_ = L;
        Crg_ = Crg;
        break;
      }
      default:
      {
        //std::cout << "Default" << std::endl;
        L_ = L;
        Crg_ = Crg;
        Cyb_ = Cyb;
        break;
      }
    }
      // std::cout << "Transformed pixel : [" << L_ << ", " 
      //                         << Crg_ << ", " 
      //                         << Cyb_ << "]" 
      //                         << std::endl;
      // std::cin.get();
  }

  return deviated_image_;
}

ColorAdjustment::~ColorAdjustment() {}