/****************************************************************
   PROGRAM:    RGB to oRGB
   AUTHOR:     Mergim Halimi   m.halimi@live.com
   DUE DATE:   

   FUNCTION:   This app is used to convert colorspace from RGB
               to oRGB and apply a warm to cool filter

   OUTPUT:     Output will be an image that contains nine images 
               inside, with the original one in middle, and 
               converted images around

   NOTES:      N/A
****************************************************************/

#include "convert_colorspace.h"
#include "color_adjustment.h"

#include <iostream>
#include <stdio.h>
#include <stdarg.h>

//Eigen Library
#include <Eigen/Dense>
#include <Eigen/Geometry>

int main(int argc, char** argv) {
  //Create class instance
  ConvertColorspace image_0_, 
                    image_1_, 
                    image_2_, 
                    image_3_, 
                    image_4_, 
                    image_5_, 
                    image_6_, 
                    image_7_;
  
  if (argc != 3) 
  {
    printf("Check the path \n ");
    return -1;
  }
  std::string input_path_ = argv[1], output_path_ = argv[2];

  // Create 1280x480 mat for window
  cv::Mat result_(cv::Size(800, 1133), CV_8UC3);

  // Copy small images into a big mat, imagine a 3x3 Matrix
  //Image (0,0)
  image_0_.setCrgCyb(-0.2, 0.2);
  image_0_.loadFromfile(input_path_);
  image_0_.RGBImage().copyTo(result_(cv::Rect(0, 0, 260, 371)));

  //Image (1,0)
  image_1_.setCrgCyb(-0.3, 0.2);
  image_1_.loadFromfile(input_path_);
  image_1_.RGBImage().copyTo(result_(cv::Rect(270, 0, 260, 371)));

  //Image (2,0)
  image_2_.setCrgCyb(-0.3, 0.1);
  image_2_.loadFromfile(input_path_);
  image_2_.RGBImage().copyTo(result_(cv::Rect(540, 0, 260, 371)));

  //Image (0,1)
  image_3_.setCrgCyb(-0.1, 0.3);
  image_3_.loadFromfile(input_path_);
  image_3_.RGBImage().copyTo(result_(cv::Rect(0, 381, 260, 371)));

  //Image (1,1) - Original Image
  image_0_.OriginalImage().copyTo(result_(cv::Rect(270, 381, 260, 371)));

  //Image (2,1)
  image_4_.setCrgCyb(-0.1, -0.1);
  image_4_.loadFromfile(input_path_);
  image_4_.RGBImage().copyTo(result_(cv::Rect(540, 381, 260, 371)));

  //Image (0,2)
  image_5_.setCrgCyb(0, 0.3);
  image_5_.loadFromfile(input_path_);
  image_5_.RGBImage().copyTo(result_(cv::Rect(0, 762, 260, 371)));

  //Image (1,2)
  image_6_.setCrgCyb(0.1, 0.2);
  image_6_.loadFromfile(input_path_);
  image_6_.RGBImage().copyTo(result_(cv::Rect(270, 762, 260, 371)));

  //Image (2,2) 
  image_7_.setCrgCyb(0.1, 0.0);
  image_7_.loadFromfile(input_path_);
  image_7_.RGBImage().copyTo(result_(cv::Rect(540, 762, 260, 371)));
    
  //Write image to disk
  cv::imwrite(output_path_, result_);

  // It displays three windows representing three channels
  //cv::namedWindow( "Channels", CV_WINDOW_NORMAL);
  //cv::imshow("Channels", image_0_.splittedIMG());

  // Display big mat
  cv::namedWindow( "Result", CV_WINDOW_FULLSCREEN);
  cv::imshow("Result", result_);

  std::cout << "Press any key to close ..." << std::endl;
  cv::waitKey(0);
  cv::destroyAllWindows();

  return 0;
}