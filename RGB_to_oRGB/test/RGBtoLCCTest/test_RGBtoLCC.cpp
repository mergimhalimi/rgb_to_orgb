/****************************************************************
   FILE:     test_RGBtoLCC.cpp
   AUTHOR:   Sahit Berisha
   DUE DATE: 20 Nov 2018
   FUNCTION: This test has to test the convert of the image
   from RGB to LCC
****************************************************************/

#include "test_RGBtoLCC.h"

extern std::string tests_data_path_;

/*
* @brief 
* @brief 
*/
TEST_F(TestRGBtoLCC, loadFromFileRightPathExpectTrue) {
  ConvertColorspace image_;

  EXPECT_TRUE(image_.loadFromfile( tests_data_path_ + "/image.png"));
}

TEST_F(TestRGBtoLCC, checkImagePixels) {

  //Initialise a class
  ConvertColorspace image_;

  image_.loadFromfile( tests_data_path_ + "/image.png");

  EXPECT_NEAR(27, image_.accessImgPixel(image_.RGBImage("CV_64FC3"), 0, 0)[0], 0.0001);
  EXPECT_NEAR(146, image_.accessImgPixel(image_.RGBImage("CV_64FC3"), 0, 0)[1], 0.0001);
  EXPECT_NEAR(100, image_.accessImgPixel(image_.RGBImage("CV_64FC3"), 0, 0)[2], 0.0001);


  EXPECT_NEAR(167.6730, image_.accessImgPixel(image_.LCCImage("CV_64FC3"), 0, 0)[2], 0.0001);
  EXPECT_NEAR(2.2490, image_.accessImgPixel(image_.LCCImage("CV_64FC3"), 0, 0)[1], 0.0001);
  EXPECT_NEAR(-142.9220, image_.accessImgPixel(image_.LCCImage("CV_64FC3"), 0, 0)[0], 0.0001);

}